#!/bin/bash 
# URL da página de releases do Fog Project no GitHub 
RELEASES_URL="https://github.com/FOGProject/fogproject/releases/latest" 
# Diretório de destino 
download_dir="fogproject" 
# Nome do arquivo de instalação 
install_script_name="installfog.sh" 
# Diretório de instalação 
install_dir="$download_dir/bin" 
# Função para exibir mensagens de erro e sair 
function exibir_erro { 
    echo "Erro: $1" 
    exit 1 
} 
# Verifica se o comando curl está disponível 
command -v curl > /dev/null || exibir_erro "O comando 'curl' não está instalado. Por favor, instale-o para continuar." 
# Obtém a última versão do Fog Project a partir da página de releases do GitHub 
echo "Obtendo a última versão do Fog Project..." 
latest_version=$(curl -sSLI -o /dev/null -w %{url_effective} "$RELEASES_URL" | grep -o "[0-9.]*$") 
# Verifica se a versão foi obtida com sucesso 
if [ -z "$latest_version" ]; then 
    exibir_erro "Não foi possível obter a versão mais recente do Fog Project." 
fi 
# Constrói a URL do arquivo compactado 
download_url="https://github.com/FOGProject/fogproject/archive/$latest_version.tar.gz" 
# Cria o diretório de destino se não existir 
mkdir -p "$download_dir" || exibir_erro "Não foi possível criar o diretório de destino." 
# Baixa e extrai a última versão 
echo "Baixando e extraindo a última versão do Fog Project..." 
curl -sSL "$download_url" | tar -xz -C "$download_dir" --strip-components=1 || exibir_erro "Ocorreu um erro durante o download ou extração da última versão do Fog Project." 
# Verifica se o download e extração foram bem-sucedidos 
echo "Download e extração concluídos com sucesso. A última versão está em: $download_dir" 
# Executa o arquivo de instalação, se existir 
install_script="$install_dir/$install_script_name" 
if [ -f "$install_script" ]; then 
    echo "Iniciando o arquivo de instalação do Fog Project..." 
    bash "$install_script" 
else 
    echo "Arquivo de instalação não encontrado. Certifique-se de que existe um arquivo $install_script_name no diretório baixado." 
fi 
# Fim do script
